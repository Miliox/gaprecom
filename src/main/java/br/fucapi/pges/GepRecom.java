package br.fucapi.pges;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.mahout.cf.taste.common.*;
import org.apache.mahout.cf.taste.recommender.*;
import org.apache.mahout.cf.taste.similarity.*;
import org.apache.mahout.cf.taste.impl.recommender.CachingRecommender;
import org.apache.mahout.cf.taste.impl.recommender.GenericItemBasedRecommender;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.AveragingPreferenceInferrer;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.UncenteredCosineSimilarity;
import org.apache.mahout.cf.taste.impl.model.jdbc.MySQLJDBCDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class GepRecom {
	private static MysqlConnectionPoolDataSource dataSource;

	static {
		dataSource = new MysqlConnectionPoolDataSource();
		((MysqlDataSource) dataSource).setUser("root");
		((MysqlDataSource) dataSource).setPassword("");
		((MysqlDataSource) dataSource).setServerName("localhost");
		((MysqlDataSource) dataSource).setPort(3306);
		((MysqlDataSource) dataSource).setDatabaseName("gaprecom");
    }

	/**
	 * @param args
	 * @throws SQLException
	 * @throws IOException
	 * @throws TasteException
	 * @throws NamingException 
	 */
	public static void main(String[] args) throws SQLException, IOException, TasteException {
		Connection conn = getConnection();
		
        // TODO: RECEIVE PARAMETER BY COMMAND LINE || HTTP GET
        int userId = Integer.parseInt(args[0]);
        
        MySQLJDBCDataModel dataModel = new MySQLJDBCDataModel(dataSource,
            "Preference", "user_id", "game_id", "user_score", "timestamp");

        UncenteredCosineSimilarity similarity = new UncenteredCosineSimilarity(dataModel);
        similarity.setPreferenceInferrer(new AveragingPreferenceInferrer(dataModel));

        UserNeighborhood neighborhood = new NearestNUserNeighborhood(5, 0.5, similarity, dataModel);

        CachingRecommender userRecommender =
            new CachingRecommender(
                new GenericUserBasedRecommender(dataModel, neighborhood, similarity));
	    CachingRecommender itemRecommender =
		        new CachingRecommender(new GenericItemBasedRecommender(dataModel, similarity));
	    
        JSONObject output = new JSONObject();
        output.put("user", toJsonArray(userRecommender.recommend(userId, 5)));
        output.put("item", toJsonArray(itemRecommender.recommend(userId, 5)));

	conn.close();

        System.out.println(output); 
	}
	
	private static JSONArray toJsonArray(List<RecommendedItem> recommendation) {
		JSONArray array = new JSONArray();
		for(RecommendedItem item : recommendation) {
        	array.add(item.getItemID());
        }
		return array;
	}


	private static Connection getConnection() throws SQLException {
		return dataSource.getConnection();
	}



}
