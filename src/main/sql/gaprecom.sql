-- MySQL dump 10.13  Distrib 5.5.24, for Win64 (x86)
--
-- Host: localhost    Database: gaprecom
-- ------------------------------------------------------
-- Server version	5.5.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Game`
--

DROP TABLE IF EXISTS `Game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Game` (
  `id_game` int(11) NOT NULL AUTO_INCREMENT COMMENT '	',
  `genre_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `studio` varchar(45) NOT NULL,
  `description` varchar(256) NOT NULL,
  PRIMARY KEY (`id_game`,`genre_id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `fk_Game_Genre1_idx` (`genre_id`),
  CONSTRAINT `fk_Game_Genre1` FOREIGN KEY (`genre_id`) REFERENCES `Genre` (`id_genre`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Game`
--

LOCK TABLES `Game` WRITE;
/*!40000 ALTER TABLE `Game` DISABLE KEYS */;
INSERT INTO `Game` VALUES (1,1,'Across Age','FDG Entertaiment',''),(2,1,'Amazing Alex','Rovio',''),(3,1,'C.H.A.O.S','Sky Jet International',''),(4,1,'Dark Knight Rises','Gameloft',''),(5,1,'Dugeon Hunter 3','Gameloft',''),(6,1,'Fruit Ninja','Halfbrick Studio',''),(7,1,'Mass Effect Infiltrator','EA',''),(8,1,'Minecraft - Pocket Edition','Mojang',''),(9,1,'Necromancer Rising','RyanMitchellGames.com',''),(10,1,'Ninja Action RPG: Ninja Royale','Mobage',''),(11,1,'N.O.V.A 3 - Near Orbit Vanguard Alliance','Gameloft',''),(12,1,'Rayman Jungle Run','Ubisoft Entertaiment',''),(13,1,'The Bard\'s Tale','inXile Entertaiment',''),(14,1,'Zombie Smash','Zynga',''),(15,1,'Zombieville USA','Mika Mobile',''),(16,2,'Astraware Casino','Handmark',''),(17,2,'Backgammon Deluxe','Mouse Games',''),(18,2,'Blackjack Free','MobilityWare',''),(19,2,'Chess Free','AI Factory Limited',''),(20,2,'Damas','Magma Mobile',''),(21,2,'Dominoes','appscraft',''),(22,2,'FreeCell','MobilityWare',''),(23,2,'Ludo','FunGamesMobile.com',''),(24,2,'Jewels Pro','Mouse Games',''),(25,2,'Resta Um','Felipse Tumonis',''),(26,2,'Slotmania','Playtika',''),(27,2,'Smart Truco','LGE Brasil',''),(28,2,'Solitaire+','Brainium Studios',''),(29,2,'UNO','Gameloft',''),(30,2,'Zynga Poker','Zynga',''),(31,3,'Baseball Superstars 2012','Gamevil',''),(32,3,'Fight Night Champion','EA',''),(33,3,'Grand Prix Story','Kairosoft',''),(34,3,'Kick Flick Soccer','Best Fun Top Free Games For Cool Kids',''),(35,3,'Ice rage','Mountain Sheep',''),(36,3,'Lane Splitter','Fractiv',''),(37,3,'Let\'s Golft! 3','Gameloft',''),(38,3,'NBA JAM','EA',''),(39,3,'Punch Hero','Gamevil',''),(40,3,'Real Racing 2','EA',''),(41,3,'Super Bikers','Gamayun Productions',''),(42,3,'Super Ski Jump','Vivid Games',''),(43,3,'Super Stickman Golf','Noodlecake Studio',''),(44,3,'Virtual Table Tennis 3D Pro','Clanfoot',''),(45,3,'Wipeout','Activision',''),(46,4,'Anomaly Warzone Earth','11 bit studios',''),(47,4,'Crystal Defenders','Square Enix',''),(48,4,'Fieldrunners','Subatomic Studios',''),(49,4,'HexDefense','Gotow.net',''),(50,4,'Greed Corp','Invictus',''),(51,4,'Jelly Defense','Infinite Dreams',''),(52,4,'LandGrabbers: Strategy Game','Nevosoft',''),(53,4,'Modern Conflict','Gaijin Entertaiment',''),(54,4,'Operation Barbarossa','Joni Nuutinen',''),(55,4,'Plant vs. Zombies','PopCap',''),(56,4,'Rebuild - A post-apocalyptic strategy game','Sarah Northway',''),(57,4,'Robotek HD','Hexagen Ltd',''),(58,4,'Sea Empire','Blue Plop Team',''),(59,4,'Sentinel 3: Homeworld','Origin8 Technologies',''),(60,4,'Tower Defense: Lost Earth','Com2uS',''),(61,5,'Abelha Estressada','Byte-Pronto Games',''),(62,5,'Angry Birds Space','Rovio',''),(63,5,'Apparatus','Bithack',''),(64,5,'Burn The Rope','Big Blue Bubble',''),(65,5,'Cogs','Lazy 8 Studios',''),(66,5,'Cut The Rope','ZeptoLab',''),(67,5,'Drawdle','One Side Softeware',''),(68,5,'Machinarium','Hothead Games',''),(69,5,'Osmos HD','Hemisphere Games',''),(70,5,'Spirits','Spaces of Play',''),(71,5,'Sudoku','All Factory Limited',''),(72,5,'The Curse','Toy Studio Media Corporation',''),(73,5,'Where\'s My Water','Disney',''),(74,5,'World of Goo','2D Boy',''),(75,5,'Zen Bound 2','Secret Exit','');
/*!40000 ALTER TABLE `Game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Genre`
--

DROP TABLE IF EXISTS `Genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Genre` (
  `id_genre` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL,
  `description` varchar(256) NOT NULL,
  PRIMARY KEY (`id_genre`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Genre`
--

LOCK TABLES `Genre` WRITE;
/*!40000 ALTER TABLE `Genre` DISABLE KEYS */;
INSERT INTO `Genre` VALUES (1,'Acao e Aventura',''),(2,'Carta e Tabuleir',''),(3,'Esporte',''),(4,'Estrategia',''),(5,'Puzzle','');
/*!40000 ALTER TABLE `Genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Preference`
--

DROP TABLE IF EXISTS `Preference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Preference` (
  `user_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `user_score` int(11) DEFAULT NULL,
  `timestamp` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`game_id`),
  KEY `fk_Preferencia_Usuario_idx` (`user_id`),
  KEY `fk_Preferencia_Jogo1_idx` (`game_id`),
  CONSTRAINT `fk_Preferencia_Jogo1` FOREIGN KEY (`game_id`) REFERENCES `Game` (`id_game`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Preferencia_Usuario` FOREIGN KEY (`user_id`) REFERENCES `User` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Preference`
--

LOCK TABLES `Preference` WRITE;
/*!40000 ALTER TABLE `Preference` DISABLE KEYS */;
INSERT INTO `Preference` VALUES (1,22,3,'1349471294'),(1,31,2,'1349471294'),(1,36,2,'1349471294'),(1,41,3,'1349471294'),(1,44,1,'1349471294'),(1,55,5,'1349471294'),(1,61,3,'1349471294'),(1,65,3,'1349471294'),(1,68,4,'1349471294'),(2,8,3,'1349474283'),(2,22,5,'1349474283'),(2,38,1,'1349474283'),(2,41,2,'1349474283'),(2,47,2,'1349474283'),(2,50,4,'1349474283'),(2,51,5,'1349474283'),(2,54,2,'1349474283'),(2,72,4,'1349474283'),(3,3,3,'1349474377'),(3,12,4,'1349474377'),(3,20,1,'1349474377'),(3,33,4,'1349474377'),(3,37,2,'1349474377'),(3,38,1,'1349474377'),(3,48,2,'1349474377'),(3,70,3,'1349474377'),(3,74,5,'1349474377'),(4,11,4,'1349474454'),(4,12,4,'1349474454'),(4,38,1,'1349474454'),(4,39,2,'1349474454'),(4,46,2,'1349474454'),(4,51,3,'1349474454'),(4,53,2,'1349474454'),(4,56,4,'1349474454'),(4,73,5,'1349474454'),(5,6,4,'1349474536'),(5,11,2,'1349474536'),(5,15,3,'1349474536'),(5,17,1,'1349474536'),(5,25,1,'1349474536'),(5,31,1,'1349474536'),(5,45,3,'1349474536'),(5,48,2,'1349474536'),(5,57,4,'1349474536'),(6,10,3,'1349474605'),(6,22,2,'1349474605'),(6,24,3,'1349474605'),(6,25,1,'1349474605'),(6,26,1,'1349474605'),(6,47,2,'1349474605'),(6,57,4,'1349474605'),(6,61,3,'1349474605'),(6,71,3,'1349474605'),(7,12,4,'1349474667'),(7,16,5,'1349474667'),(7,20,4,'1349474667'),(7,30,5,'1349474667'),(7,38,2,'1349474667'),(7,48,3,'1349474667'),(7,60,3,'1349474667'),(7,65,2,'1349474667'),(7,70,3,'1349474667'),(8,1,2,'1349474725'),(8,12,3,'1349474725'),(8,15,3,'1349474725'),(8,21,4,'1349474725'),(8,26,1,'1349474725'),(8,40,3,'1349474725'),(8,41,2,'1349474725'),(8,45,3,'1349474725'),(8,59,5,'1349474725'),(9,4,4,'1349474822'),(9,13,4,'1349474822'),(9,17,2,'1349474822'),(9,42,1,'1349474822'),(9,45,2,'1349474822'),(9,58,1,'1349474822'),(9,68,5,'1349474822'),(9,74,4,'1349474822'),(9,75,3,'1349474822'),(10,14,4,'1349474897'),(10,17,2,'1349474897'),(10,28,4,'1349474897'),(10,31,1,'1349474897'),(10,36,2,'1349474897'),(10,53,3,'1349474897'),(10,55,4,'1349474897'),(10,56,3,'1349474897'),(10,69,4,'1349474897'),(11,1,1,'1349474946'),(11,9,3,'1349474946'),(11,33,2,'1349474946'),(11,38,1,'1349474946'),(11,43,1,'1349474946'),(11,49,3,'1349474946'),(11,52,1,'1349474946'),(11,57,4,'1349474946'),(11,72,4,'1349474946'),(12,17,2,'1349475014'),(12,27,3,'1349475014'),(12,36,1,'1349475014'),(12,40,3,'1349475014'),(12,42,1,'1349475014'),(12,52,2,'1349475014'),(12,67,4,'1349475014'),(12,71,3,'1349475014'),(12,74,4,'1349475014'),(13,15,3,'1349475083'),(13,16,2,'1349475083'),(13,28,3,'1349475083'),(13,39,2,'1349475083'),(13,46,3,'1349475083'),(13,59,2,'1349475083'),(13,63,4,'1349475083'),(13,69,5,'1349475083'),(13,70,4,'1349475083'),(14,10,5,'1349475140'),(14,25,2,'1349475140'),(14,26,1,'1349475140'),(14,35,3,'1349475140'),(14,36,2,'1349475140'),(14,51,4,'1349475140'),(14,56,3,'1349475140'),(14,57,4,'1349475140'),(14,72,4,'1349475140'),(15,1,2,'1349475202'),(15,4,2,'1349475202'),(15,10,3,'1349475202'),(15,21,2,'1349475202'),(15,23,2,'1349475202'),(15,35,3,'1349475202'),(15,38,1,'1349475202'),(15,61,4,'1349475202'),(15,63,4,'1349475202'),(16,5,4,'1349475266'),(16,7,4,'1349475266'),(16,13,3,'1349475266'),(16,21,5,'1349475266'),(16,24,3,'1349475266'),(16,38,3,'1349475266'),(16,46,3,'1349475266'),(16,59,2,'1349475266'),(16,70,2,'1349475266'),(17,4,4,'1349475347'),(17,5,5,'1349475347'),(17,16,3,'1349475347'),(17,25,2,'1349475347'),(17,33,4,'1349475347'),(17,44,2,'1349475347'),(17,67,2,'1349475347'),(17,70,4,'1349475347'),(17,72,4,'1349475347'),(18,9,3,'1349475420'),(18,14,4,'1349475420'),(18,23,1,'1349475420'),(18,24,2,'1349475420'),(18,42,1,'1349475420'),(18,46,4,'1349475420'),(18,58,2,'1349475420'),(18,61,3,'1349475420'),(18,64,5,'1349475420'),(19,1,2,'1349475511'),(19,6,4,'1349475511'),(19,17,2,'1349475511'),(19,18,3,'1349475511'),(19,19,1,'1349475511'),(19,29,4,'1349475511'),(19,32,1,'1349475511'),(19,36,2,'1349475511'),(19,58,1,'1349475511');
/*!40000 ALTER TABLE `Preference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Recomendation`
--

DROP TABLE IF EXISTS `Recomendation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Recomendation` (
  `id_recom` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_score` int(10) unsigned NOT NULL,
  `type` char(1) NOT NULL COMMENT 'type: User based (U) or Item Based (I)',
  PRIMARY KEY (`id_recom`,`user_id`),
  KEY `fk_Recomendation_User1_idx` (`user_id`),
  CONSTRAINT `fk_Recomendation_User1` FOREIGN KEY (`user_id`) REFERENCES `User` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Recomendation`
--

LOCK TABLES `Recomendation` WRITE;
/*!40000 ALTER TABLE `Recomendation` DISABLE KEYS */;
/*!40000 ALTER TABLE `Recomendation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RecommendedGame`
--

DROP TABLE IF EXISTS `RecommendedGame`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RecommendedGame` (
  `recom_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  PRIMARY KEY (`recom_id`,`game_id`),
  KEY `fk_Recomendation_has_Game_Game1_idx` (`game_id`),
  KEY `fk_Recomendation_has_Game_Recomendation1_idx` (`recom_id`),
  CONSTRAINT `fk_Recomendation_has_Game_Game1` FOREIGN KEY (`game_id`) REFERENCES `Game` (`id_game`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Recomendation_has_Game_Recomendation1` FOREIGN KEY (`recom_id`) REFERENCES `Recomendation` (`id_recom`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RecommendedGame`
--

LOCK TABLES `RecommendedGame` WRITE;
/*!40000 ALTER TABLE `RecommendedGame` DISABLE KEYS */;
/*!40000 ALTER TABLE `RecommendedGame` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tag`
--

DROP TABLE IF EXISTS `Tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Tag` (
  `id_tag` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id_tag`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tag`
--

LOCK TABLES `Tag` WRITE;
/*!40000 ALTER TABLE `Tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tag_has_Game`
--

DROP TABLE IF EXISTS `Tag_has_Game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Tag_has_Game` (
  `tag_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  PRIMARY KEY (`tag_id`,`game_id`),
  KEY `fk_Tag_has_Game_Game1_idx` (`game_id`),
  KEY `fk_Tag_has_Game_Tag1_idx` (`tag_id`),
  CONSTRAINT `fk_Tag_has_Game_Game1` FOREIGN KEY (`game_id`) REFERENCES `Game` (`id_game`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tag_has_Game_Tag1` FOREIGN KEY (`tag_id`) REFERENCES `Tag` (`id_tag`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tag_has_Game`
--

LOCK TABLES `Tag_has_Game` WRITE;
/*!40000 ALTER TABLE `Tag_has_Game` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tag_has_Game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `age` tinyint(3) unsigned zerofill NOT NULL,
  `sex` char(1) NOT NULL COMMENT 'sex: M or F',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (1,'Emiliano Firmino',024,'M'),(2,'Joao Guilherme Martinez',023,'M'),(3,'Diego Lins Freitas',027,'M'),(4,'Fran Maia',027,'F'),(5,'Silvia Oliveira',021,'F'),(6,'Nilson Oliveira',022,'M'),(7,'Raphael Oliveira',024,'M'),(8,'George Dourado',027,'M'),(9,'Iuri Vieira',024,'M'),(10,'Sebastiao Mouco',036,'M'),(11,'Fabiola Noronha',024,'F'),(12,'Vitoria Regina',021,'F'),(13,'Josiane Rodrigues',021,'F'),(14,'Jeyson Moreira',020,'M'),(15,'Yasmine Gabriele',023,'F'),(16,'George Nunes',026,'M'),(17,'Bruno Mendes',023,'M'),(18,'Thiago Fernandes',024,'M'),(19,'Polyanna Henrique',021,'F');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-10-05 18:19:49
