SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `gaprecom` ;
CREATE SCHEMA IF NOT EXISTS `gaprecom` DEFAULT CHARACTER SET latin1 ;
USE `gaprecom` ;

-- -----------------------------------------------------
-- Table `gaprecom`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gaprecom`.`User` ;

CREATE  TABLE IF NOT EXISTS `gaprecom`.`User` (
  `id_user` INT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `age` TINYINT UNSIGNED ZEROFILL NOT NULL ,
  `sex` CHAR(1) NOT NULL COMMENT 'sex: M or F' ,
  PRIMARY KEY (`id_user`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gaprecom`.`Genre`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gaprecom`.`Genre` ;

CREATE  TABLE IF NOT EXISTS `gaprecom`.`Genre` (
  `id_genre` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(16) NOT NULL ,
  `description` VARCHAR(256) NOT NULL ,
  PRIMARY KEY (`id_genre`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gaprecom`.`Game`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gaprecom`.`Game` ;

CREATE  TABLE IF NOT EXISTS `gaprecom`.`Game` (
  `id_game` INT NULL AUTO_INCREMENT COMMENT '	' ,
  `genre_id` INT NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  `studio` VARCHAR(45) NOT NULL ,
  `description` VARCHAR(256) NOT NULL ,
  PRIMARY KEY (`id_game`, `genre_id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) ,
  INDEX `fk_Game_Genre1_idx` (`genre_id` ASC) ,
  CONSTRAINT `fk_Game_Genre1`
    FOREIGN KEY (`genre_id` )
    REFERENCES `gaprecom`.`Genre` (`id_genre` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gaprecom`.`Preference`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gaprecom`.`Preference` ;

CREATE  TABLE IF NOT EXISTS `gaprecom`.`Preference` (
  `user_id` INT NOT NULL ,
  `game_id` INT NOT NULL ,
  `user_score` INT NULL ,
  `timestamp` VARCHAR(45) NULL ,
  PRIMARY KEY (`user_id`, `game_id`) ,
  INDEX `fk_Preferencia_Usuario_idx` (`user_id` ASC) ,
  INDEX `fk_Preferencia_Jogo1_idx` (`game_id` ASC) ,
  CONSTRAINT `fk_Preferencia_Usuario`
    FOREIGN KEY (`user_id` )
    REFERENCES `gaprecom`.`User` (`id_user` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Preferencia_Jogo1`
    FOREIGN KEY (`game_id` )
    REFERENCES `gaprecom`.`Game` (`id_game` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gaprecom`.`Recomendation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gaprecom`.`Recomendation` ;

CREATE  TABLE IF NOT EXISTS `gaprecom`.`Recomendation` (
  `id_recom` INT NOT NULL AUTO_INCREMENT ,
  `user_id` INT NOT NULL ,
  `user_score` INT UNSIGNED NOT NULL ,
  `type` CHAR(1) NOT NULL COMMENT 'type: User based (U) or Item Based (I)' ,
  PRIMARY KEY (`id_recom`, `user_id`) ,
  INDEX `fk_Recomendation_User1_idx` (`user_id` ASC) ,
  CONSTRAINT `fk_Recomendation_User1`
    FOREIGN KEY (`user_id` )
    REFERENCES `gaprecom`.`User` (`id_user` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gaprecom`.`RecommendedGame`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gaprecom`.`RecommendedGame` ;

CREATE  TABLE IF NOT EXISTS `gaprecom`.`RecommendedGame` (
  `recom_id` INT NOT NULL ,
  `game_id` INT NOT NULL ,
  PRIMARY KEY (`recom_id`, `game_id`) ,
  INDEX `fk_Recomendation_has_Game_Game1_idx` (`game_id` ASC) ,
  INDEX `fk_Recomendation_has_Game_Recomendation1_idx` (`recom_id` ASC) ,
  CONSTRAINT `fk_Recomendation_has_Game_Recomendation1`
    FOREIGN KEY (`recom_id` )
    REFERENCES `gaprecom`.`Recomendation` (`id_recom` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Recomendation_has_Game_Game1`
    FOREIGN KEY (`game_id` )
    REFERENCES `gaprecom`.`Game` (`id_game` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gaprecom`.`Tag`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gaprecom`.`Tag` ;

CREATE  TABLE IF NOT EXISTS `gaprecom`.`Tag` (
  `id_tag` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id_tag`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gaprecom`.`Tag_has_Game`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gaprecom`.`Tag_has_Game` ;

CREATE  TABLE IF NOT EXISTS `gaprecom`.`Tag_has_Game` (
  `tag_id` INT NOT NULL ,
  `game_id` INT NOT NULL ,
  PRIMARY KEY (`tag_id`, `game_id`) ,
  INDEX `fk_Tag_has_Game_Game1_idx` (`game_id` ASC) ,
  INDEX `fk_Tag_has_Game_Tag1_idx` (`tag_id` ASC) ,
  CONSTRAINT `fk_Tag_has_Game_Tag1`
    FOREIGN KEY (`tag_id` )
    REFERENCES `gaprecom`.`Tag` (`id_tag` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tag_has_Game_Game1`
    FOREIGN KEY (`game_id` )
    REFERENCES `gaprecom`.`Game` (`id_game` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
