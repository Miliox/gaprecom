<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en_US" xml:lang="en_US">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="Content-Language" content="en" />
<title>Sistema de recomendação de jogos</title>
<style type="text/css">
td {background-color:#e0e0e0;}
tr {background-color:#f0f0f0;}
.botao {
background: #1e5799; /* Old browsers */
background: -moz-linear-gradient(top,  #1e5799 0%, #2989d8 50%, #207cca 51%, #7db9e8 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#1e5799), color-stop(50%,#2989d8), color-stop(51%,#207cca), color-stop(100%,#7db9e8)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #1e5799 0%,#2989d8 50%,#207cca 51%,#7db9e8 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #1e5799 0%,#2989d8 50%,#207cca 51%,#7db9e8 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #1e5799 0%,#2989d8 50%,#207cca 51%,#7db9e8 100%); /* IE10+ */
background: linear-gradient(to bottom,  #1e5799 0%,#2989d8 50%,#207cca 51%,#7db9e8 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1e5799', endColorstr='#7db9e8',GradientType=0 ); /* IE6-9 */
width:100px; height:30px; font-size:14px; float:right; color:#fff; border:0; font-weight:bold;
}
h2,h3 {color:#eee;}
</style>
<script type="text/javascript">
function valida() {
	var d = document.formulario;
	
	if(d.nome.value == "") {
		alert('Preencha com seu nome');
		return false;
	} else if(d.idade[d.idade.selectedIndex].value == "") {
		alert('Preencha com sua idade');
		return false
	} else if(d.sexo[0].checked == false && d.sexo[1].checked == false) {
		alert('Preencha com seu sexo');
		return false;
	} else if(d.jogo1[d.jogo1.selectedIndex].value == "") {
		alert('Avalie o jogo 1');
		return false;
	} else if(d.jogo2[d.jogo2.selectedIndex].value == "") {
		alert('Avalie o jogo 2');
		return false;
	} else if(d.jogo3[d.jogo3.selectedIndex].value == "") {
		alert('Avalie o jogo 3');
		return false;
	} else if(d.jogo4[d.jogo4.selectedIndex].value == "") {
		alert('Avalie o jogo 4');
		return false;
	} else if(d.jogo5[d.jogo5.selectedIndex].value == "") {
		alert('Avalie o jogo 5');
		return false;
	} else if(d.jogo6[d.jogo6.selectedIndex].value == "") {
		alert('Avalie o jogo 6');
		return false;
	} else if(d.jogo7[d.jogo7.selectedIndex].value == "") {
		alert('Avalie o jogo 7');
		return false;
	} else if(d.jogo8[d.jogo8.selectedIndex].value == "") {
		alert('Avalie o jogo 8');
		return false;
	} else if(d.jogo9[d.jogo9.selectedIndex].value == "") {
		alert('Avalie o jogo 9');
		return false;
	}
}
</script>
</head>
<?php require 'functions.php'; ?>
<body style="background-color:#333; font-family: Tahoma;">

	<form name="formulario" action="salva_recomendacao.php" method="post" onSubmit="return valida()">
		<div style="width:800px; margin:0 auto;">
		<h2>Sistema de recomendação de jogos para Smartphones</h2>
		<table bgcolor="#fff" cellpadding="10">
			<tr>
				<td>Nome</td>
				<td colspan="4"><input type="text" name="nome" value="" size="70"/></td>
			</tr>
			<tr>
				<td>Idade</td>
				<td width="115">
				  <select name="idade">
				    <option value=""></option>
				  <?php
				  	for($i=13;$i<=50;$i++) {
				  		?><option value="<?php echo $i; ?>"> <?php echo $i; ?> </option> <?php
				  	} ?>
				  </select>
				</td>
				<td>Sexo</td>
				<td width="457">
					<input type="radio" name="sexo" value="M" /> Masculino 
					<input type="radio" name="sexo" value="F" /> Feminino
				</td>
			</tr>
		</table>
		<br/><br/>
		<table bgcolor="#fff" cellpadding="10">
			<tr>
				<td><b>Logo</b></td>
				<td><b>Nome</b></td>
				<td><b>Gênero</b></td>
                <!-- <td><b>Descrição</b></td> -->
				<td><b>Nota</b></td>
			</tr>
		<?php $jogos = busca_jogos();
			foreach($jogos as $key => $jogo) {  ?>
				<tr>
					<td><img src="<?php echo 'imgs/' . $jogo['id'] . '.jpg'; ?>"></td> 
                  <td><b><?php echo $jogo['title']; ?></b></td>
					<td><?php echo $jogo['genre']; ?></td>
					<!--<td><-?php echo $jogo['descricao']; ?></td>-->
					<td>Nota:
						<select name="jogo<?php echo $key+1; ?>">
						  <option value=""></option>
						  <option value="1">1</option>
						  <option value="2">2</option>
						  <option value="3">3</option>
						  <option value="4">4</option>
						  <option value="5">5</option>					  
						</select>
						<input type="hidden" name="id_jogo<?php echo $key+1; ?>" value="<?php echo $jogo['id']; ?>">
					</td>
				</tr>
			<?php } ?>
		</table>
		<br/>
		<input type="submit" name="submit" class="botao" value="Enviar"/><br/><br/>&nbsp;
		</div>
	</form>
</body>
</html>
