<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en_US" xml:lang="en_US">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="Content-Language" content="en" />
<title>Sistema de recomendação de jogos</title>
<style type="text/css">
td {background-color:#e0e0e0;}
tr {background-color:#f0f0f0;}
.botao {
background: #1e5799; /* Old browsers */
background: -moz-linear-gradient(top,  #1e5799 0%, #2989d8 50%, #207cca 51%, #7db9e8 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#1e5799), color-stop(50%,#2989d8), color-stop(51%,#207cca), color-stop(100%,#7db9e8)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #1e5799 0%,#2989d8 50%,#207cca 51%,#7db9e8 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #1e5799 0%,#2989d8 50%,#207cca 51%,#7db9e8 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #1e5799 0%,#2989d8 50%,#207cca 51%,#7db9e8 100%); /* IE10+ */
background: linear-gradient(to bottom,  #1e5799 0%,#2989d8 50%,#207cca 51%,#7db9e8 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1e5799', endColorstr='#7db9e8',GradientType=0 ); /* IE6-9 */
width:100px; height:30px; font-size:14px; float:right; color:#fff; border:0; font-weight:bold;
}
h2,h3 {color:#eee;}
</style>
<script type="text/javascript">
function valida() {
	var d = document.formulario;
	if(d.avaliacao[d.avaliacao.selectedIndex].value == "") {
		alert('Avalie a recomendação');
		return false;
	}
}
</script>
</head>
<?php require 'functions.php'; ?>
<body style="background-color:#333; font-family: Tahoma;">

	<form name="formulario" action="salva_avaliacao.php" method="post" onSubmit="return valida()">
		<div style="width:800px; margin:0 auto;">
		<h2>Sistema de recomendação de jogos para Smartphones</h2>
		<h3>Nome: <?php echo $_GET['name']; ?> </h3>
		<br/><br/>
		<table bgcolor="#fff" cellpadding="10">
			<tr>
				<td colspan="4" align="center"><b>Jogos Recomendados #1</b></td>
			</tr>
			<?php $recomendacao = busca_recomendacao($_GET['id']); 
			foreach($recomendacao["user"] as $key => $jogo) {  ?>
				<tr>
					<td><img src="<?php echo 'imgs/'. $jogo['gid'] . '.jpg'; ?>"></td>
					<td><b><?php echo $jogo['title']; ?></b></td>
					<td><?php echo $jogo['genre']; ?></td>
				</tr>
			<?php } ?>
			<tr>
				<td colspan="4" align="right"><b>Nota</b>
				        <select name="avaliacao1">
						  <option value=""></option>
						  <option value="1">1</option>
						  <option value="2">2</option>
						  <option value="3">3</option>
						  <option value="4">4</option>
						  <option value="5">5</option>					  
						</select>
				</td>
			</tr>
			<tr>
				<td colspan="4" align="center"><b>Jogos Recomendados #2</b></td>
			</tr>
			<?php foreach($recomendacao["item"] as $key => $jogo) {  ?>
				<tr>
					<td><img src="<?php echo 'imgs/'. $jogo['gid'] . '.jpg'; ?>"></td>
					<td><b><?php echo $jogo['title']; ?></b></td>
					<td><?php echo $jogo['genre']; ?></td>
				</tr>
			<?php } ?>
			<tr>
				<td colspan="4" align="right"><b>Nota</b>
				        <select name="avaliacao2">
						  <option value=""></option>
						  <option value="1">1</option>
						  <option value="2">2</option>
						  <option value="3">3</option>
						  <option value="4">4</option>
						  <option value="5">5</option>					  
						</select>
				</td>
			</tr>
			<input type="hidden" name="userRecomendacao" value="<?php echo $recomendacao["user"][0]['id']; ?>">
			<input type="hidden" name="itemRecomendacao" value="<?php echo $recomendacao["item"][0]['id']; ?>">
		</table>
		<input type="submit" name="submit_avaliacao" class="botao" value="Enviar"/><br/><br/>&nbsp;
		</div>
	</form>
</body>
</html>
