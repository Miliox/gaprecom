<?php

function conecta_mysql() {
	mysql_connect('localhost','root','');
	mysql_select_db('gaprecom');
}

function busca_jogos() {
	conecta_mysql();
	$result = mysql_query(
		'SELECT Game.id_game as id, Game.name as title, Game.description as description, Genre.name as genre ' . 
		'FROM Game ' .
		'INNER JOIN Genre ' . 
		'ON Genre.id_genre = Game.genre_id '. 
		'ORDER BY RAND() LIMIT 15');
	$array = array();
	while($row = mysql_fetch_assoc($result)) {
		$array[] = $row;
	}
	mysql_close();
	return $array;
}

function busca_recomendacao($id) {
	//DON'T USE EXEC!!!
	$json = json_decode(exec("java -jar gapp.jar " . $id));
	
	$values = '';
	if (count($json->user) < 1 && count($json->item) < 1) {
		return array();
	}
	
	conecta_mysql();
	mysql_query(
		'INSERT INTO Recomendation (user_id, type) ' .
		'VALUES (' . $id . ', "U")'
	);
	$ru_id = mysql_insert_id();
	foreach($json->user as $item) {
		mysql_query(
			'INSERT INTO RecommendedGame (recom_id, game_id) ' .
		    'VALUES (' . $ru_id  . ', ' . $item . ')');
	}
	
	mysql_query(
		'INSERT INTO Recomendation (user_id, type) ' .
		'VALUES (' . $id . ', "I")'
	);
	$ri_id = mysql_insert_id();
	foreach($json->item as $item) {
		mysql_query(
			'INSERT INTO RecommendedGame (recom_id, game_id) ' .
		    'VALUES (' . $ri_id  . ', ' . $item . ')');
	}
	
	$sql = 'SELECT g.name as title, gr.name as genre, g.id_game as gid, r.id_recom as id ' . 
	    'FROM Recomendation as r ' . 
	    'INNER JOIN RecommendedGame as rg ON rg.recom_id = r.id_recom ' .
	    'INNER JOIN Game as g ON g.id_game = rg.game_id ' . 
	    'INNER JOIN Genre as gr ON gr.id_genre = g.genre_id ' .
	    'WHERE r.id_recom = ';
	
	$user_recommendation = mysql_query($sql . $ru_id);
	$item_recommendation = mysql_query($sql . $ri_id);
	
	$user_games = array();
	while($row = mysql_fetch_assoc($user_recommendation)) {
		$user_games[] = $row;
	}
	$item_games = array();
	while($row = mysql_fetch_assoc($item_recommendation)) {
		$item_games[] = $row;
	}
	
	mysql_close();
	
	return array('user' => $user_games, 'item' => $item_games);		
}

?>
